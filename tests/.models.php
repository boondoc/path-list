<?php
	namespace	Boondoc\Model;

	use		Boondoc\PathList;


	class	DerpBoring		{ use PathList; };
	class	DerpStringsOwn		{ use PathList;  public function __construct (string ...$paths) { $this->pathsSetOwn ($paths); } };
	class	DerpStringsShared	{ use PathList;  public function __construct (string ...$paths) { static::pathsSet ($paths); } };

	class	DerpStringsSharedChild	extends DerpStringsShared {};

