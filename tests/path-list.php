<?php
	namespace	Boondoc\Tests\Units;

	use		atoum;
	use		ArrayObject;
	use		DateTimeImmutable;
	use		Boondoc\Model\DerpBoring;
	use		Boondoc\Model\DerpStringsOwn;
	use		Boondoc\Model\DerpStringsShared;
	use		Boondoc\Model\DerpStringsSharedChild;

	use		TypeError;
	use		Boondoc\PathList\NoRealPathsSupplied;


	class PathList extends atoum
	{
		public	const			 TESTFAKE_1	= ['subdir_1', 'subdir_4', 'subdir_7'];
		public	const			 TESTREAL_1	= ['subdir_2', 'subdir_5', 'subdir_8'];
		public	const			 TESTREAL_2	= ['subdir_3', 'subdir_6', 'subdir_9', 'subdir_0'];

		public	const			 TESTFILE	=  'findme.txt';


		public function setUp		() { array_map ('mkdir', array_merge (static::TESTREAL_1, static::TESTREAL_2)); }
		public function tearDown	() { array_map ('rmdir', array_merge (static::TESTREAL_1, static::TESTREAL_2)); }

		public function beforeTestMethod (string $method)
		{
			if ($method === 'testFind')
				touch (static::TESTREAL_1[1] . DIRECTORY_SEPARATOR . static::TESTFILE);
		}

		public function afterTestMethod (string $method)
		{
			if ($method === 'testFind')
				unlink (static::TESTREAL_1[1] . DIRECTORY_SEPARATOR . static::TESTFILE);
		}


		private function fullpath (string $relative = null) : string
		{
			$path					=  getcwd () . ($relative ? (DIRECTORY_SEPARATOR . $relative) : '');
			$this	->dump				($path);
			return $path;
		}


		public function testRealsies ()
		{
			$db					=  new DerpBoring;

			$db	::pathsSet			(array_merge	(static::TESTREAL_1,
										 static::TESTFAKE_1,
										 static::TESTREAL_2));

			$this	->assert			('All paths fully-qualified, non-existent paths discarded')
				->array				($db::pathsGet ())
				->hasSize			( count (static::TESTREAL_1) + count (static::TESTREAL_2))
				->notContains			( static::TESTREAL_1[0])
				->contains			($this->fullpath (static::TESTREAL_1[0]))
				->notContains			( static::TESTFAKE_1[0])
				->notContains			($this->fullpath (static::TESTFAKE_1[0]));
		}

		public function testFakesies ()
		{
			$db					=  new DerpBoring ();

			$this	->assert			('Nonsense variable type as path')
				->exception			( fn () => $db::pathsSet (array (new DateTimeImmutable ())))
				->isInstanceOf			( TypeError::class)

				->assert			('Array of only non-existent paths')
				->exception			( fn () => $db->pathsSetOwn (static::TESTFAKE_1))
				->isInstanceOf			( NoRealPathsSupplied::class)

				->assert			('Non-existent paths passed by unpacking')
				->exception			( fn () => new DerpStringsOwn (...static::TESTFAKE_1))
				->isInstanceOf			( NoRealPathsSupplied::class)

				->assert			('Empty array passed by unpacking')
				->given				($dso = new DerpStringsOwn ())
				->array				($dso->pathsGetOwn ())
				->isEmpty			();
		}

		public function testGet ()
		{
			$db					=  new DerpBoring;

			$db	::pathsSet			( static::TESTREAL_1);
			$db	->pathsSetOwn			( static::TESTREAL_2);

			$this	->assert			('Should return shared absolute path statically')
				->string			($db::pathsGet (static::TESTREAL_1[1]))
				->isNotEqualTo			( static::TESTREAL_1[1])
				->isEqualTo			($this->fullpath (static::TESTREAL_1[1]))

				->assert			('Should return false if not shared')
				->boolean			($db::pathsGet (static::TESTREAL_2[1]))
				->isFalse			()

				->assert			('Should return false if not own')
				->boolean			($db->pathsGetOwn (static::TESTREAL_1[1]))
				->isFalse			()

				->assert			('Should return own absolute path individually')
				->string			($db->pathsGetOwn (static::TESTREAL_2[1]))
				->isNotEqualTo			( static::TESTREAL_2[1])
				->isEqualTo			($this->fullpath (static::TESTREAL_2[1]))

				->assert			('Should return absolute path from all')
				->string			($db->pathsGetAll (static::TESTREAL_1[1]))
				->isNotEqualTo			( static::TESTREAL_1[1])
				->isEqualTo			($this->fullpath (static::TESTREAL_1[1]))
				->string			($db->pathsGetAll (static::TESTREAL_2[1]))
				->isNotEqualTo			( static::TESTREAL_2[1])
				->isEqualTo			($this->fullpath (static::TESTREAL_2[1]))

				->assert			('Should return false if never added')
				->boolean			($db::pathsGet ($this->fullpath ()))
				->isFalse			()
				->boolean			($db->pathsGetOwn ($this->fullpath ()))
				->isFalse			()
				->boolean			($db->pathsGetAll ($this->fullpath ()))
				->isFalse			();
		}

		public function testHas ()
		{
			$db					=  new DerpBoring;

			$db	::pathsSet			(static::TESTREAL_1);
			$db	->pathsSetOwn			(static::TESTREAL_2);

			$this	->assert			('Should has shared statically')
				->boolean			($db::pathsHas ($this->fullpath (static::TESTREAL_1[2])))
				->isTrue			()
				->boolean			($db::pathsHas (realpath (static::TESTREAL_1[2])))
				->isTrue			()
				->boolean			($db::pathsHas (static::TESTREAL_1[2]))
				->isTrue			()

				->assert			('Should not has own statically')
				->boolean			($db::pathsHas ($this->fullpath (static::TESTREAL_2[2])))
				->isFalse			()
				->boolean			($db::pathsHas (realpath (static::TESTREAL_2[2])))
				->isFalse			()
				->boolean			($db::pathsHas (static::TESTREAL_2[2]))
				->isFalse			()

				->assert			('Should not has shared individually')
				->boolean			($db->pathsHasOwn ($this->fullpath (static::TESTREAL_1[2])))
				->isFalse			()
				->boolean			($db->pathsHasOwn (realpath (static::TESTREAL_1[2])))
				->isFalse			()
				->boolean			($db->pathsHasOwn (static::TESTREAL_1[2]))
				->isFalse			()

				->assert			('Should has own individually')
				->boolean			($db->pathsHasOwn ($this->fullpath (static::TESTREAL_2[2])))
				->isTrue			()
				->boolean			($db->pathsHasOwn (realpath (static::TESTREAL_2[2])))
				->isTrue			()
				->boolean			($db->pathsHasOwn (static::TESTREAL_2[2]))
				->isTrue			()

				->assert			('Should has shared in all')
				->boolean			($db->pathsHasAll ($this->fullpath (static::TESTREAL_1[2])))
				->isTrue			()
				->boolean			($db->pathsHasAll (realpath (static::TESTREAL_1[2])))
				->isTrue			()
				->boolean			($db->pathsHasAll (static::TESTREAL_1[2]))
				->isTrue			()

				->assert			('Should has own in all')
				->boolean			($db->pathsHasAll ($this->fullpath (static::TESTREAL_2[2])))
				->isTrue			()
				->boolean			($db->pathsHasAll (realpath (static::TESTREAL_2[2])))
				->isTrue			()
				->boolean			($db->pathsHasAll (static::TESTREAL_2[2]))
				->isTrue			()

				->assert			('Should not has if never added')
				->boolean			($db::pathsHas ($this->fullpath ()))
				->isFalse			()
				->boolean			($db->pathsHasOwn ($this->fullpath ()))
				->isFalse			()
				->boolean			($db->pathsHasAll ($this->fullpath ()))
				->isFalse			();
		}

		public function testClear ()
		{
			$db					=  new DerpBoring;

			$db	::pathsSet			(static::TESTREAL_1);
			$db	->pathsSetOwn			(static::TESTREAL_2);

			$this	->assert			('Should has shared statically')
				->boolean			($db::pathsHas ($this->fullpath (static::TESTREAL_1[2])))
				->isTrue			()
				->boolean			($db::pathsHas (static::TESTREAL_1[2]))
				->isTrue			()

				->assert			('Should has own individually')
				->boolean			($db->pathsHasOwn ($this->fullpath (static::TESTREAL_2[2])))
				->isTrue			()
				->boolean			($db->pathsHasOwn (static::TESTREAL_2[2]))
				->isTrue			();

			$db	::pathsSet			([]);

			$this	->assert			('Should no longer has shared statically')
				->boolean			($db::pathsHas ($this->fullpath (static::TESTREAL_1[2])))
				->isFalse			()
				->boolean			($db::pathsHas (static::TESTREAL_1[2]))
				->isFalse			()
				->array				($db::pathsGet ())
				->isIdenticalTo			( array ())

				->assert			('Should still has own individually')
				->boolean			($db->pathsHasOwn ($this->fullpath (static::TESTREAL_2[2])))
				->isTrue			()
				->boolean			($db->pathsHasOwn (static::TESTREAL_2[2]))
				->isTrue			();

			$db	::pathsSet			(static::TESTREAL_1);
			$db	->pathsSetOwn			([]);

			$this	->assert			('Should again has shared statically')
				->boolean			($db::pathsHas ($this->fullpath (static::TESTREAL_1[2])))
				->isTrue			()
				->boolean			($db::pathsHas (static::TESTREAL_1[2]))
				->isTrue			()
				->array				($db::pathsGet ())

				->assert			('Should still has own individually')
				->boolean			($db->pathsHasOwn ($this->fullpath (static::TESTREAL_2[2])))
				->isFalse			()
				->boolean			($db->pathsHasOwn (static::TESTREAL_2[2]))
				->isFalse			()
				->array				($db->pathsGetOwn ())
				->isIdenticalTo			( array ());
		}

		public function testNested ()
		{
			$db_flat				=  new DerpBoring ();
			$db_nest				=  new DerpBoring ();

			$db_flat->pathsSetOwn			(array ('subdir_2', 'subdir_5', 'subdir_8', 'subdir_3', 'subdir_6', 'subdir_9', 'subdir_0'));
			$db_nest->pathsSetOwn			(array
			(
				'subdir_2',

				array
				(
					42,

					array
					(
						'subdir_5',
					),

					'subdir_8',
					 true,
					'subdir_3',

					new ArrayObject (array
					(
						array
						(
							'subdir_6',
						),

						-16.6666666667,
					)),
				),

				'subdir_9',
				 false,
				'subdir_0',
			));

			$this	->assert			('Nested path collection should flatten automatically')
				->array				($db_nest->pathsGetOwn ())
				->isIdenticalTo			($db_flat->pathsGetOwn ());
		}

		public function testIterator ()
		{
			$db					=  new DerpBoring ();
			$paths_real				=  new ArrayObject (array_combine (static::TESTFAKE_1, static::TESTREAL_1));
			$paths_fake				=  new ArrayObject (array_combine (static::TESTREAL_1, static::TESTFAKE_1));

			$db	::pathsSet			($paths_real);

			$this	->assert			('Traversable object as input')
				->array				($db::pathsGet ())
				->notContains			( static::TESTREAL_1[0])
				->contains			($this->fullpath (static::TESTREAL_1[0]))
				->notContains			( static::TESTFAKE_1[0])
				->notContains			($this->fullpath (static::TESTFAKE_1[0]))

				->assert			('Should not has from array keys')
				->boolean			($db::pathsHas (static::TESTFAKE_1[1]))
				->isFalse			()

				->assert			('Should has from array values')
				->boolean			($db::pathsHas (static::TESTREAL_1[1]))
				->isTrue			()

				->assert			('Get from array values')
				->string			($db::pathsGet (static::TESTREAL_1[2]))
				->isEqualTo			($this->fullpath (static::TESTREAL_1[2]))

				->assert			('Empty traversable object')
				->exception			( fn () => $db::pathsSet ($paths_fake))
				->isInstanceOf			( NoRealPathsSupplied::class);
		}

		public function testSeparateOwn ()
		{
			$dso1					=  new DerpStringsOwn		(...static::TESTREAL_1);
			$dso2					=  new DerpStringsOwn		(...static::TESTREAL_2);

			$dso1	::pathsSet			([$this->fullpath ()]);

			$this	->assert			('Two instances of same class share static list, separate own lists')

				->array				($dso1::pathsGet ())
				->contains			($this->fullpath ())
				->notContains			($this->fullpath (static::TESTREAL_1[1]))
				->notContains			($this->fullpath (static::TESTREAL_2[1]))

				->array				($dso2::pathsGet ())
				->contains			($this->fullpath ())
				->notContains			($this->fullpath (static::TESTREAL_1[1]))
				->notContains			($this->fullpath (static::TESTREAL_2[1]))

				->array				($dso1->pathsGetAll ())
				->contains			($this->fullpath ())
				->contains			($this->fullpath (static::TESTREAL_1[1]))
				->notContains			($this->fullpath (static::TESTREAL_2[1]))

				->array				($dso2->pathsGetAll ())
				->contains			($this->fullpath ())
				->notContains			($this->fullpath (static::TESTREAL_1[1]))
				->contains			($this->fullpath (static::TESTREAL_2[1]));
		}

		public function testSharedOrOwn ()
		{
			$dso					=  new DerpStringsOwn		(...static::TESTREAL_1);
			$dss					=  new DerpStringsShared	(...static::TESTREAL_1);

			$this	->assert			('Own class contains no shared paths')
				->array				($dso::pathsGet ())
				->hasSize			( 0)
				->boolean			($dso::pathsHas (static::TESTREAL_1[0]))
				->isFalse			()

				->assert			('Shared class contains shared paths')
				->array				($dss::pathsGet ())
				->hasSize			( count (static::TESTREAL_1))
				->contains			($this->fullpath (static::TESTREAL_1[0]))
				->boolean			($dss::pathsHas (static::TESTREAL_1[0]))
				->isTrue			()

				->assert			('Own class contains own paths')
				->array				($dso->pathsGetOwn ())
				->hasSize			( count (static::TESTREAL_1))
				->contains			($this->fullpath (static::TESTREAL_1[0]))
				->boolean			($dso->pathsHasOwn (static::TESTREAL_1[0]))
				->isTrue			()

				->assert			('Shared class contains no own paths')
				->array				($dss->pathsGetOwn ())
				->hasSize			( 0)
				->boolean			($dss->pathsHasOwn (static::TESTREAL_1[0]))
				->isFalse			()

				->assert			('Both classes export identical “All” lists')
				->array				($dso->pathsGetAll ())
				->isIdenticalTo			($dss->pathsGetAll ());
		}

		public function testSharedInheritance ()
		{
			$dss					=  new DerpStringsShared	(...static::TESTREAL_1);
			$dsc					=  new DerpStringsSharedChild	(...static::TESTREAL_2);

			$this	->assert			('Parent class contains only parent paths and no child paths')
				->array				($dss->pathsGetAll ())
				->contains			($this->fullpath (static::TESTREAL_1[0]))
				->notContains			($this->fullpath (static::TESTREAL_2[0]))
				->boolean			($dss->pathsHasAll (static::TESTREAL_1[0]))
				->isTrue			()
				->boolean			($dss->pathsHasAll (static::TESTREAL_2[0]))
				->isFalse			()

				->assert			('Child class contains only child paths and no parent paths')
				->array				($dsc->pathsGetAll ())
				->notContains			($this->fullpath (static::TESTREAL_1[0]))
				->contains			($this->fullpath (static::TESTREAL_2[0]))
				->boolean			($dsc->pathsHasAll (static::TESTREAL_1[0]))
				->isFalse			()
				->boolean			($dsc->pathsHasAll (static::TESTREAL_2[0]))
				->isTrue			();
		}

		public function testFind ()
		{
			$dso					=  new DerpStringsOwn		(...static::TESTREAL_1);
			$dss					=  new DerpStringsShared	(...static::TESTREAL_1);

			$target					= $this->fullpath (static::TESTREAL_1[1] . DIRECTORY_SEPARATOR . static::TESTFILE);

			$this	->assert			('Own class finds target file in own paths')
				->boolean			($dso->pathsFind	(static::TESTFILE))
				->isFalse			()
				->string			($dso->pathsFindOwn	(static::TESTFILE))
				->isIdenticalTo			($target)
				->string			($dso->pathsFindAll	(static::TESTFILE))
				->isIdenticalTo			($target)

				->assert			('Shared class finds target file in shared paths')
				->string			($dss->pathsFind	(static::TESTFILE))
				->isIdenticalTo			($target)
				->boolean			($dss->pathsFindOwn	(static::TESTFILE))
				->isFalse			()
				->string			($dss->pathsFindAll	(static::TESTFILE))
				->isIdenticalTo			($target);
		}
	};

