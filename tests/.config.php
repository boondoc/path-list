<?php
	use		atoum\atoum\test\generator;
	use		atoum\atoum\report\fields\runner\coverage\html	as coverage;

	$runner	->setTestGenerator		((new generator ())
			->setTestedClassNamespace	('Boondoc')
			// ->setTestClassNamespace		('Boondoc\Test')
			->setTestClassesDirectory	(__DIR__))
		->addTestsFromDirectory		(__DIR__);

	$banner					=  trim (fgets (fopen ('README.md', 'r')), "# \n\r\t\v\x00");

	extension_loaded ('xdebug')		&&
	$script	->enableBranchAndPathCoverage	()
		->addDefaultReport		()
		->addField			((new coverage ($banner, '.report'))
			->setRootUrl			(realpath (__DIR__ . '/../.report/index.html')));

