<?php
	namespace	Boondoc;

	use		Boondoc\PathList\NoRealPathsSupplied;


	trait PathList
	{
		protected	static		 array		$pathsShared		=  array ();
		protected			 array		$pathsOwn		=  array ();


		private		static		function	 pathResolve		(string $path)		: ?string
		{
			return realpath (trim ($path)) ?: null;
		}

		private		static		function	 pathsFlatten		(iterable $paths)	:  array
		{
			$flattened				=  array ();

			// Iterator classes look neat and tidy, but they provide no way to recurse into iterable sub-elements
			// while passing over non-iterable objects, and they take twice the processing time anyway.
			$walker					=  function (iterable|string $node) use (&$flattened, &$walker) : void
			{
				if (is_iterable ($node))
				{
					if (is_object ($node))
						$node					=  iterator_to_array ($node, false);

					array_walk_recursive ($node, $walker);
				}
				elseif ($node)
					$flattened[]				=  static::pathResolve ($node);
			};

			$walker ($paths);

			return array_values (array_unique (array_filter ($flattened)));
		}

		private		static		function	 pathsSearch		(array  $paths,
											 string $filename)	:  string|false
		{
			foreach ($paths as $path)
			{
				$found					= $path . DIRECTORY_SEPARATOR . $filename;

				if (file_exists ($found))
					return $found;
			}

			return false;
		}


		public		static		function	 pathsGet		(string $path = null)	:  array|string|false
		{
			if (!array_key_exists (static::class, static::$pathsShared))
				static::pathsSet (array ());

			if (empty ($path))
				return static::$pathsShared[static::class];

			$resolved				=  static::pathResolve ($path);

			return	  $resolved && in_array ($resolved, static::$pathsShared[static::class])
				? $resolved
				:  false;
		}

		public		static		function	 pathsSet		(iterable $paths)	:  void
		{
			$flattened				=  static::pathsFlatten ($paths);

			// Only complain if path values were supplied and could not be resolved
			if (empty ($flattened) and iterator_count ($paths))
				throw new NoRealPathsSupplied ($paths);

			static::$pathsShared[static::class]	= $flattened;
		}

		public		static		function	 pathsHas		(string $path)		:  bool
		{
			return !empty (static::pathsGet ($path));
		}

		public		static		function	 pathsFind		(string $filename)	:  string|false
		{
			return static::pathsSearch (static::pathsGet (), $filename);
		}


		public				function	 pathsGetOwn		(string $path = null)	:  array|string|false
		{
			if (empty ($path))
				return $this->pathsOwn;

			$resolved				=  static::pathResolve ($path);

			return	  $resolved && in_array ($resolved, $this->pathsOwn)
				? $resolved
				:  false;
		}

		public				function	 pathsSetOwn		(iterable $paths)	:  void
		{
			$flattened				=  static::pathsFlatten ($paths);

			// Only complain if path values were supplied and could not be resolved
			if (empty ($flattened) and iterator_count ($paths))
				throw new NoRealPathsSupplied ($paths);

			$this->pathsOwn				= $flattened;
		}

		public				function	 pathsHasOwn		(string $path)		:  bool
		{
			return !empty ($this->pathsGetOwn ($path));
		}

		public				function	 pathsFindOwn		(string $filename)	:  string|false
		{
			return static::pathsSearch ($this->pathsGetOwn (), $filename);
		}


		public				function	 pathsGetAll		(string $path = null)	:  array|string|false
		{
			if (empty ($path))
				return array_unique (array_filter (array_merge ($this->pathsGetOwn (), static::pathsGet ())));

			return $this->pathsGetOwn ($path) ?: static::pathsGet ($path);
		}

		public				function	 pathsHasAll		(string $path)		:  bool
		{
			return static::pathsHas ($path) || $this->pathsHasOwn ($path);
		}

		public				function	 pathsFindAll		(string $filename)	:  string|false
		{
			return static::pathsSearch ($this->pathsGetAll (), $filename);
		}
	};



	namespace	Boondoc\PathList;

	use		Throwable;
	use		UnexpectedValueException;


	abstract class Exception extends UnexpectedValueException
	{
		protected			mixed		$value;

		public				function	   getValue	() : mixed
		{
			return $this->value;
		}

		public				function	 __construct	(string $message, mixed $value, int $code = 0, Throwable $previous = null)
		{
			$this->value				= $value;

			parent::__construct (sprintf ($message, $value), $code, $previous);
		}
	};


	class NoRealPathsSupplied extends Exception
	{
		public				function	 __construct	(iterable $paths, int $code = 0, Throwable $previous = null)
		{
			parent::__construct ('None of the supplied file paths were resolvable: “%s”',
				implode ('”, “', iterator_to_array ($paths, false)), $code, $previous);
		}
	};

